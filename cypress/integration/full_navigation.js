describe("Full app navigation", function () {
  it("Should visit app", function () {
    cy.visit("http://localhost:3000");
    cy.contains("Lot Gállego Santiago. All rights reserved.");
  });
  it("Should type text in the input", function () {
    cy.get(".chakra-input").type("Arizona");
  });
  it("Should add a marker in Arizona", function () {
    cy.get(".pac-item").first().click();
  });
  it("Should add marker", function () {
    cy.wait(1000);
    cy.get("[aria-label='Map']").click(390, 250);
  });
});
