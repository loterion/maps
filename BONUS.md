Fent servir polimorfisme afegim el mètode `getPrice()` a la classe `Service` que retorni 0

```
Service.prototype.getPrice = function(){
  return 0;
};
```

A les subclasses `StreamingService` i `DownloadService` que retornin els seus preus respectivament.

```
StreamingService.prototype.getPrice = function(){
  return this.multimediaContent.streamingPrice;
};

DownloadService.prototype.getPrice = function(){
  return this.multimediaContent.downloadPrice;
};
```

Amb la mateixa aproximació de la classe `Service` afegim un mètode `getAditionalFee()`

```
MultimediaContent.prototype.getAditionalFee = function(){
  return 0;
}
```

A la class `MultimediaContent` i a la class `PremiumContent`

```
PremiumContent.prototype.getAditionalFee = function(){
  return this.additionalFee;
}
```

Amb els nous mètodes de les classes `Service` i `MultimediaContent`
afegim el valor retornat per aquests mètodes al total

```
class RegisteredUser{

  ...

  getTotal(){
    let total = 0;

    this.services.forEach((service, index) => {
      let multimediaContent = service.getMultimediaContent();
      total += service.getPrice();
      total += multimediaContent.getAditionalFee();
    })

    return total;
  }
}
```
