# Add markers to google maps

To local execution of this app it's necessary to create a .env file in the project root directory with the key for google api services
`REACT_APP_GOOGLE_KEY=XXXXXXXXXXXXXXXXXXXXXXX`

This App let you find addresses using google autocomplete and add markers by clicking in the map.

This project uses the following third party packages:

- **google-map-react**
- **react-geocode** (transform coords to addresses and addresses to coords)
- **react-autocomplete** (manage the addresses suggestions)
- **chakra-ui** (UI library)
- **redux-toolkit** (manage state)
- **cypress** (e2e testing)

## Demo

You can visit the [demo](https://maps-loterion.vercel.app/) deployed at **vercel**

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.
You will also see any lint errors in the console.

### `npm test`

Launches the test with cypress.

### `npm run build`

Builds the app for production to the `build` folder.
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.
