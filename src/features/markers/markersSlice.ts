import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import type { RootState } from "store";

interface IMarkersState {
  text: string;
  lat: number;
  lng: number;
}

const initialState: IMarkersState[] = [
  {
    text: "Manresa",
    lat: 41.7211121,
    lng: 1.8164403,
  },
];

export const markerSlice = createSlice({
  name: "markers",
  initialState,
  reducers: {
    addMarker: (state, action: PayloadAction<IMarkersState>) => {
      state.push(action.payload);
    },
  },
});

export const { addMarker } = markerSlice.actions;
export const selectMarkers = (state: RootState) => state.markers;
export default markerSlice.reducer;
