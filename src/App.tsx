import React from "react";
import Map from "components/Map";
import SearchBox from "components/SearchBox";
import Footer from "components/Footer";

function App() {
  return (
    <div className="App">
      <SearchBox />
      <Map />
      <Footer />
    </div>
  );
}

export default App;
