import React from "react";
import {
  Box,
  Input,
  InputGroup,
  InputLeftElement,
  Icon,
} from "@chakra-ui/react";
import { FaLocationArrow } from "react-icons/fa";
import { usePlacesWidget } from "react-google-autocomplete";
import Geocode from "react-geocode";
import { useAppDispatch } from "hooks";
import { addMarker } from "features/markers/markersSlice";

Geocode.setApiKey(process.env.REACT_APP_GOOGLE_KEY!);

function SearchBox() {
  const dispatch = useAppDispatch();

  const { ref: autoCompleteRef } = usePlacesWidget<HTMLInputElement>({
    apiKey: process.env.REACT_APP_GOOGLE_KEY,
    onPlaceSelected: (place) => {
      Geocode.fromAddress(place.formatted_address).then(
        (response) => {
          const { formatted_address } = response.results[0];
          const { lat, lng } = response.results[0].geometry.location;
          dispatch(addMarker({ lat, lng, text: formatted_address }));
        },
        (error) => {
          console.log(error);
        }
      );
    },
  });

  return (
    <Box p={4} color="white">
      <InputGroup>
        <InputLeftElement
          pointerEvents="none"
          children={<Icon as={FaLocationArrow} color="gray.300" />}
        />
        <Input
          type="tel"
          placeholder="Location"
          color="gray.900"
          ref={autoCompleteRef}
        />
      </InputGroup>
    </Box>
  );
}

export default SearchBox;
