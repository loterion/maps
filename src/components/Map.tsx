import React from "react";
import { Box } from "@chakra-ui/react";
import GoogleMapReact from "google-map-react";
import Geocode from "react-geocode";
import Marker from "components/Marker";
import { useAppDispatch, useAppSelector } from "hooks";
import { addMarker, selectMarkers } from "features/markers/markersSlice";

Geocode.setApiKey(process.env.REACT_APP_GOOGLE_KEY!);

function Map() {
  const markers = useAppSelector(selectMarkers);
  const lastMarker = markers.slice(-1)[0];
  const dispatch = useAppDispatch();

  //transform address to coordinates and add to the state
  const handleMapClick = (e: any) => {
    const { lat, lng } = e;
    Geocode.fromLatLng(lat, lng).then((response) => {
      const text = response?.results.length
        ? response.results.find(
            (i: any) => i.types[0] === "administrative_area_level_2"
          ).formatted_address
        : "";
      dispatch(addMarker({ lat, lng, text }));
    });
  };

  return (
    <Box w="100vw" h="90vh">
      <GoogleMapReact
        bootstrapURLKeys={{
          key: process.env.REACT_APP_GOOGLE_KEY!,
          libraries: ["places"],
        }}
        defaultCenter={{ lat: markers[0].lat, lng: markers[0].lng }}
        defaultZoom={9}
        center={{ lat: lastMarker.lat, lng: lastMarker.lng }}
        onClick={handleMapClick}
      >
        {markers.map((m, i) => (
          <Marker key={`marker${i}`} lat={m.lat} lng={m.lng} text={m.text} />
        ))}
      </GoogleMapReact>
    </Box>
  );
}

export default Map;
