import React from "react";
import { Tooltip, Box } from "@chakra-ui/react";

type IMarker = {
  lat: number;
  lng: number;
  text: string;
};

const Marker = ({ text }: IMarker) => {
  return (
    <Box style={{ transform: "translate(-50%, -100%)", position: "absolute" }}>
      <Tooltip hasArrow label={text} placement="top" rounded={4} p={2}>
        <svg
          width="48"
          height="48"
          viewBox="0 0 48 48"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            d="M17.5 20C17.5 16.4101 20.4101 13.5 24 13.5C27.5899 13.5 30.5 16.4101 30.5 20C30.5 23.5899 27.5899 26.5 24 26.5C20.4101 26.5 17.5 23.5899 17.5 20Z"
            fill="#D53232"
          />
          <path
            fillRule="evenodd"
            clipRule="evenodd"
            d="M7.54709 17.7548C8.23436 9.41689 15.2019 3 23.5681 3H24.432C32.7982 3 39.7657 9.41689 40.453 17.7548C40.8231 22.244 39.4364 26.7016 36.5851 30.1886L26.9989 41.9122C25.449 43.8078 22.5511 43.8078 21.0012 41.9122L11.415 30.1887C8.56373 26.7016 7.17705 22.244 7.54709 17.7548ZM24 10.5C18.7533 10.5 14.5 14.7533 14.5 20C14.5 25.2467 18.7533 29.5 24 29.5C29.2467 29.5 33.5 25.2467 33.5 20C33.5 14.7533 29.2467 10.5 24 10.5Z"
            fill="#D53232"
          />
          <circle cx="24" cy="20" r="8" stroke="white" strokeWidth="4" />
        </svg>
      </Tooltip>
    </Box>
  );
};

export default Marker;
