# Change Log

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [1.0.5] - 2024-01-23

Add BONUS.md file with the answers to theory questions in prueba_tecnica_frontend_iskra_catala.pptx

### Added

- BONUS.md

## [1.0.4] - 2024-01-23

Add changelog file

### Added

- Changelog file

## [1.0.3] - 2024-01-23

### Added

- Deployment in vercel

## [1.0.2] - 2024-01-23

### Changed

- Readme

## [1.0.1] - 2023-01-23

Complet full end-to-end testing with cypress

### Added

- Install cypress
- Setup cypress commands
- Create full navigation testing
-

## [1.0.0] - 2022-01-23

Completed full app functionality

### Added

- Google map
- Chakra UI
- Autocomplete input
- Markers on map

### Changed

### Fixed
